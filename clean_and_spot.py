#!/usr/bin/env python3
# coding:utf-8
'''
robotest.py - Test the features of BreezyCreate2

This code is part of BreezyCreate2

The MIT License

Copyright (c) 2016 Simon D. Levy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
'''

from breezycreate2 import Robot
import time




def clean_and_spot(bot):

    # 16ビット２の歩数表現されたビット列をintに直す
    f = lambda value: -(value & 0b1000000000000000) | (value & 0b0111111111111111)
    # bot.setForwardSpeed(+50)
    bot.setSafeMode()
    t_time = 0.0

    # # Report bumper hits and wall proximity for 30 seconds
    start_time = time.time()

    CLEAN_MODE = 1
    SPOT_MODE = 2

    bot.setCleanMode()
    bot_mode = CLEAN_MODE

    enter_clean_time = time.time()
    exit_clean_time = -1

    enter_spot_time = -1
    exit_spot_time = -1

    print 'ML開始: ' + str(start_time)
    while (time.time() - start_time) < 35:
        c_time = time.time()

        if bot_mode == CLEAN_MODE:
            vw = None
            try:
                vw = bot.getViatualWall()
            except UnicodeDecodeError:
                if vw == None:
                    vw = False

            if vw:
                print 'VW検知:'
                if c_time - exit_spot_time > 10:
                    bot.setSafeMode()
                    bot_mode = SPOT_MODE
                    enter_spot_time = c_time
                    exit_clean_time = c_time
                    bot.setSpotMode()
                    print 'BM変更: CLEAN => SPOT'

                else:
                    print 'VW検知: 最後にスポットモードを脱してからまだ10秒以上経っていません'

        elif bot_mode == SPOT_MODE:
            if c_time - enter_spot_time > 10:
                print 'BM変更: SPOT => CLEAN'
                bot.setCleanMode()
                bot_mode = CLEAN_MODE
                enter_spot_time = c_time
                exit_spot_time = c_time

    print 'ML終了: ' + str(time.time())


if __name__ == '__main__':
    import sys

    bot = Robot('COM3',115200)
    try:
        clean_and_spot(bot)
    except KeyboardInterrupt as e:
        bot.setSafeMode()
        bot.close()


    

