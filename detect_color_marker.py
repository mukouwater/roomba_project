import cv2
import numpy as np

def find_rects_of_target_color(image):
  hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV_FULL)
  h = hsv[:, :, 0]
  s = hsv[:, :, 1]
  v = hsv[:, :, 2]
  print h.shape,s.shape,v.shape
  mask = np.zeros(h.shape, dtype=np.uint8)
  # mask[((h < 5) | (h > 250)) & ((150 < s)) & (30 < v)] = 255
  # mask[((h < 15) | (h > 240)) & ((120 < s) & (s < 150))] = 255
  mask[((h < 15) | (h > 240)) & ((80 < s))] = 255

  
  contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
  rects = []
  for contour in contours:
    approx = cv2.convexHull(contour)
    rect = cv2.boundingRect(approx)
    rects.append(np.array(rect))
  return rects

if __name__ == "__main__":
  capture = cv2.VideoCapture(1)
  print capture.isOpened()

  w,h = 640,480
  capture.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH,w)
  capture.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT,h)


  
  exit()
  while cv2.waitKey(30) < 0:
    _, frame = capture.read()    
    rects = find_rects_of_target_color(frame)
    if len(rects) > 0:
      rect = max(rects, key=(lambda x: x[2] * x[3]))
      if rect[2]*rect[3] > 1000:
        cv2.rectangle(frame, tuple(rect[0:2]), tuple(rect[0:2] + rect[2:4]), (0, 0, 255), thickness=2)

    cv2.imshow('red', frame)
    
  capture.release()
  cv2.destroyAllWindows()
