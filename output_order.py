

import sys


def output_last_command():
    orders = sys.argv[1:]
    with open("/home/user/Desktop/roomba_demo/last_done_command.sh","w") as f:
        f.write(' '.join(orders))
    
if __name__ == '__main__':
    output_last_command()
