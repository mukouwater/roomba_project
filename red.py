import cv2
import numpy as np
import time

from spoit import flatten_image

def find_rects_of_target_color(image):
  hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV_FULL)
  h,s,v = cv2.split(hsv)
  mask = np.zeros(h.shape, dtype=np.uint8)
  mask[((h < 15) | (h > 160)) & ((s > 80 )) & ((v > 150))] = 255
  cv2.imshow("red mask",mask)
  contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
  rects = []
  for contour in contours:
    approx = cv2.convexHull(contour)
    rect = cv2.boundingRect(approx)
    rects.append(np.array(rect))
  return rects


def find_rects_of_blue(image):
  hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV_FULL)
  h,s,v = cv2.split(hsv)
  mask = np.zeros(h.shape, dtype=np.uint8)
  mask[((150 < h) & (h < 180)) & ((s > 30)) & ((v < 200) & (v > 30))  ] = 255
  cv2.imshow("blue mask",mask)
  contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
  rects = []
  for contour in contours:
    approx = cv2.convexHull(contour)
    rect = cv2.boundingRect(approx)
    rects.append(np.array(rect))
  return rects

def find_rects_of_marker(image):

  blue_rects = find_rects_of_blue(image)
  blue_rects = filter(lambda x: x[2] * x[3] > 500, blue_rects)
  marker_rects = []
  
  if len(blue_rects)> 0:
    red_rects = find_rects_of_target_color(image)
    red_rects = filter(lambda x: x[2] * x[3] > 250, red_rects)
    if len(red_rects) > 0:
      for rr in red_rects:
        for br in blue_rects:
          outer_left_x, outer_top_y, outer_right_x, outer_bottom_y = br[0], br[1], br[0] + br[2], br[1] + br[3]
          inner_left_x, inner_top_y, inner_right_x, inner_bottom_y = rr[0], rr[1], rr[0] + rr[2], rr[1] + rr[3]
          inner_flag_x = outer_left_x < inner_left_x and inner_right_x < outer_right_x
          inner_flag_y = outer_top_y < inner_top_y and inner_bottom_y < outer_bottom_y
          if inner_flag_x and inner_flag_y:
            marker_rects.append((br,rr))
            break

    for outer_blue_rect, inner_red_rect in marker_rects:
      cv2.rectangle(image, tuple(inner_red_rect[0:2]), tuple(inner_red_rect[0:2] + inner_red_rect[2:4]), (0, 0, 255), thickness=2)
      cv2.rectangle(image, tuple(outer_blue_rect[0:2]), tuple(outer_blue_rect[0:2] + outer_blue_rect[2:4]), (120, 0, 25), thickness=2)

  return marker_rects


if __name__ == "__main__":
  capture = cv2.VideoCapture(0)
  if capture.isOpened() is False:
    capture = cv2.VideoCapture(1)
    if capture.isOpened() is False:
      raise("IO Error")

  w,h = 640,480
  w,h = 160,120
  capture.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH,w)
  capture.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT,h)

  while cv2.waitKey(30) < 0:
    _, frame = capture.read()   
    cv2.imshow('src', frame)
    # frame = flatten_image(frame)
    # frame = cv2.GaussianBlur(frame, (25,25), 1)    
    # inner_rects = find_rects_of_marker(frame)
    inner_rects = find_rects_of_target_color(frame)
    cv2.imshow('red', frame)
      
  capture.release()
  cv2.destroyAllWindows()
