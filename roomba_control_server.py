
# coding: utf-8

import time
import datetime
import logging

from breezycreate2 import Robot
import os

import cv2
import time
import numpy as np
import sys
from red import find_rects_of_target_color,find_rects_of_marker


class Marker_Search_Robot(Robot):
    def __init__(self, port='/dev/ttyUSB0', baud=115200, writeTimeout = 0):
        super().__init__(port,baud,writeTimeout)

    
class Dumot:
    def driveTest(self,velocity,radius):
        logging.debug("Dumot: velocity=",velocity," radius:",radius)

    def __init__(self, port='/dev/ttyUSB0', baud=115200, writeTimeout = 0):
        pass

# 画面底辺の中央を原点として，範囲1で正規化したuv座標系におけるマーカの存在する座標を示す
# -0.5 <= u,v <= +0.5
# u: uv座標系におけるマーカのu座標
# v: uv座標系におけるマーカのv座標
#        +1.0
#          v
#          ↑
#          | 
# -0.5 ----.----→ u +0.5
#         0.0

def get_marker_uv(capture):
    u,v = None,None
    if cv2.waitKey(30) < 0:
        _, frame = capture.read()
        rects = find_rects_of_marker(frame)
        found = len(rects) > 0
        w = float(capture.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
        h = float(capture.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
        
        if found:
          rect = max(rects, key=(lambda x: x[2] * x[3]))
          if rect[2] * rect[3] > 100:
              m = rect[0:2] + rect[2:4] / 2
              u = m[0] / w - 0.5
              v = - m[1] / h
              cv2.rectangle(frame, tuple(rect[0:2]), tuple(rect[0:2] + rect[2:4]), (0, 0, 255), thickness=2)
              cv2.rectangle(frame, tuple(m - np.array([10,10])), tuple(m + np.array([10,10])), (0, 255, 0), thickness=2)
              
        if windowFlag:
            cv2.imshow('red', frame)

    return found,u,v
    

def get_red_object(capture):
    angle = None
    if cv2.waitKey(30) < 0:
        _, frame = capture.read()
        rects = find_rects_of_target_color(frame)

        w = float(capture.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
        h = float(capture.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
        
        if len(rects) > 0:
          rect = max(rects, key=(lambda x: x[2] * x[3]))
          if rect[2] * rect[3] > 100:
              m = rect[0:2] + rect[2:4] / 2
              angle = m[0] / w - 0.5
              cv2.rectangle(frame, tuple(rect[0:2]), tuple(rect[0:2] + rect[2:4]), (0, 0, 255), thickness=2)
              cv2.rectangle(frame, tuple(m - np.array([10,10])), tuple(m + np.array([10,10])), (0, 255, 0), thickness=2)
              
        if windowFlag:
            cv2.imshow('red', frame)

    return angle

global windowFlag

def trace(bot,capture):


    logging.debug("waiting-enter")
    endFlag = False
    (
        WAIT_ORDER,
        TRACE_ORDER,
        GO_STRAIGHT_ORDER,
        RESTART_ORDER,
        SEARCH_ORDER,
        EXIT_ORDER,
    ) = range(0,6)

    # capture.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH,160)
    # capture.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT,120)

    order_id = WAIT_ORDER
    last_frame_order_id = WAIT_ORDER
    last_order_changed_time = time.time()
    try:
        while endFlag is False:
            
            # 一秒に一回指示を読み込む
            with open("C:\Users\user\engine","r") as f:
                order_id = int(f.readline())
                logging.debug(order_id)

            # エッジ処理ブロック
            if last_frame_order_id !=  order_id:
                # 今このifブロック内ではorder_idは新しく出された命令のidを意味している
                # 例えば「elif order_id == SEARCH_ORDER:」のブロックは，
                # さっきまでは別の命令が~/engineに書き込まれていたのに，今openしてみたらSEARCH_ORDERが書き込まれていた
                # （つまり新たな命令が書き込まれた）ことを意味している
                last_order_changed_time = time.time()
                if last_frame_order_id == TRACE_ORDER:
                    cv2.destroyAllWindows()
                    bot.driveTest(0,0)

                elif last_frame_order_id == GO_STRAIGHT_ORDER:
                    bot.driveTest(0,0)

                if order_id == GO_STRAIGHT_ORDER:
                    bot.driveTest(-100,0)

                elif order_id == SEARCH_ORDER:
                    bot.setCleanMode()
                    last_marker_founded_time = time.time()

            start = time.time()

            # 一秒間は今読み取ったorderに従う
            while time.time() - start < 1:
                if order_id == TRACE_ORDER:
                    red_angle = get_red_object(capture) or 0
                    v,r = 200,-red_angle * 3.5
                    if red_angle == 0:
                        v,r = 0,0
                    bot.driveTest(v,r)

                elif order_id == SEARCH_ORDER:
                    # この命令の実行中は，基本的にはCleanモード
                    # （マーカを発見してから10秒間はSpotモード）
                    marker_found_mode_time = time.time() - last_marker_founded_time
                    
                    # # marker_found_mode_time ~= 10
                    # if marker_found_mode_time - 10 < 0.001:
                    #     bot.setCleanMode()

                    if marker_found_mode_time < 15:
                        # 最後にマーカを発見しその上に乗り上げてから10秒間はその近辺の重点的な掃除のみを行う
                        pass
                    
                    else:

                        # リアルタイムは無理っぽいから10秒に1回マーカ探索を行うことにする
                        # in every 10 minutes look arround to find the marker 
                        if int(marker_found_mode_time) % 10 == 0:
                            for i in range(0,8):
                                bot.setTurnSpeed(80)
                                time.sleep(0.2)
                                bot.setTurnSpeed(0)
                                time.sleep(0.1)
                                found,u,v = get_marker_uv(capture)
                                if found:
                                    break
                                
                            # found,u,v = get_marker_uv(capture)
                            if found:
                                bot.setSafeMode()
                                time.sleep(0.1)
                                bot.setTurnSpeed(u * 100)
                                time.sleep(2)
                                bot.setTurnSpeed(0)
                                time.sleep(0.1)
                                bot.setForwardSpeed(v * 100)

                                # complete move onto the marker
                                bot.setSpotMode()
                                time.sleep(15)
                                
                                bot.setCleanMode()
                                last_marker_founded_time = time.time()

                elif order_id == EXIT_ORDER:
                    endFlag = True
                    break
                last_frame_order_id = order_id

        bot.driveTest(0,0)
        cv2.destroyAllWindows()
        logging.debug("waiting-exit")
        
    except KeyboardInterrupt:
        logging.debug('-- Ctrl-C pushed --')
        
    finally:
        logging.debug('-- release resources --')
        capture.release()
        cv2.destroyAllWindows()

        bot.driveTest(0,0)
        bot.setSafeMode()    
        bot.setForwardSpeed(0)
        bot.setTurnSpeed(0)
        bot.close()
        logging.debug("trace_red-d.py finished")

if __name__ == '__main__':

    logging.basicConfig(
        format='[%(threadName)s][%(levelname)s]%(message)s',
        levpel=logging.DEBUG
    )

    if len(sys.argv) < 3:
        print('usage: python trace_red-d.py <port_name> start|stop windowOn')
        exit()

    global port_name
    global windowFlag

    port_name = sys.argv[1]
    windowFlag = True if sys.argv[3] == "windowOn" else False
    
    try:
        bot = Robot(port_name,115200)
        # bot = Dumot(port_name,115200)
    except:
        print"Connection Error"
        sys.exit()
        
    capture = cv2.VideoCapture(0)
    if capture.isOpened() == False:
        raise("IO Error")

    trace(bot,capture)
    
