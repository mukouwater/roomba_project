#!/usr/bin/env python3

'''
robotest.py - Test the features of BreezyCreate2

This code is part of BreezyCreate2

The MIT License

Copyright (c) 2016 Simon D. Levy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
'''

from breezycreate2 import Robot
import time


def interpret(port_name,orders):

    # Create a Create2. This will automatically try to connect to your robot over serial
    bot = Robot(port_name,115200)

    
    for i in range(len(orders)):
        order = orders[i]
        if order == 'straight':
            speed = int(orders[i + 1])
            interval = int(orders[i + 2])
            i += 2
            
            bot.setForwardSpeed(speed)
            time.sleep(interval)
            bot.setForwardSpeed(0)

        elif order == 'turn':
            speed = int(orders[i + 1])
            interval = int(orders[i + 2])
            i += 2
            
            bot.setTurnSpeed(speed)
            time.sleep(interval)
            bot.setTurnSpeed(0)

        elif order == 'beep':
            bot.setCleanMode()
            time.sleep(1.6)
            bot.setSafeMode()
            
        time.sleep(0.1)
    bot.setSafeMode()
    bot.close()


if __name__ == '__main__':
    import sys
    if len(sys.argv) <= 1:
        print 'usage: python simple_deeds.py <PORT_NAME> <order1> (<order2> ... )'
        exit()

    port_name = sys.argv[1]
    orders = sys.argv[2:]
    
    interpret(sys.argv[1],orders)


    

