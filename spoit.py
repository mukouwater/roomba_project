#coding: utf-8

import cv2
import numpy as np

def flatten_image(src):
    h,w,c = src.shape
    pts1 = np.float32([[w * 0.25,h*0.4],[w * 0.75,h*0.4],[0,h],[w,h]])
    pts2 = np.float32([[0,0],[w,0],[0,h],[w,h]])
    M = cv2.getPerspectiveTransform(pts1,pts2)
    return cv2.warpPerspective(src,M,(w,h))

if __name__ == "__main__":
    capture = cv2.VideoCapture(0)
    if(capture.isOpened() is not True):
        raise("IO Error")
    
    while cv2.waitKey(30) < 0:
        _, frame = capture.read()
        src = frame

        src = flatten_image(src)

        hsv = cv2.cvtColor(src, cv2.COLOR_BGR2HSV)
        # average_square = (25, 25)
        # sigma_x = 1
        # hsv = cv2.GaussianBlur(hsv, average_square, sigma_x)
        hsv = cv2.GaussianBlur(hsv, (5,5), 1)
        
        h, s, v = cv2.split(hsv)
        red_mask = np.zeros(h.shape, dtype=np.uint8)
        red_mask[((h < 15) | (h > 160)) & (s > 150) & (v > 150)] = 255


        print src.shape,hsv.shape,red_mask.shape
        
        dst = cv2.bitwise_and(src,src, mask = red_mask)
        # print s.shape,hsv[s>15].shape,mask.shape
        cv2.imshow("frame",dst)
        cv2.imshow("s",red_mask)


