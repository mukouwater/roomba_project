
# coding: utf-8

import threading
import time
import datetime
import signal
import logging
from breezycreate2 import Robot
import os

import cv2
import time
import numpy as np
import sys
from red import find_rects_of_target_color

def get_red_object(capture):
    angle = None
    
    if cv2.waitKey(30) < 0:
        _, frame = capture.read()
        rects = find_rects_of_target_color(frame)

        w = float(capture.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
        h = float(capture.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
    
        if len(rects) > 0:
          rect = max(rects, key=(lambda x: x[2] * x[3]))
          if rect[2] * rect[3] > 100:
              m = rect[0:2] + rect[2:4] / 2
              angle = m[0] / w - 0.5
              cv2.rectangle(frame, tuple(rect[0:2]), tuple(rect[0:2] + rect[2:4]), (0, 0, 255), thickness=2)
              cv2.rectangle(frame, tuple(m - np.array([10,10])), tuple(m + np.array([10,10])), (0, 255, 0), thickness=2)

              
        if windowFlag:
            cv2.imshow('red', frame)

    return angle

global startFlag
global windowFlag

def trace(bot,capture):

    try:
        if startFlag:
            logging.debug("waiting-enter")
            endFlag = False
            (
                WAIT_ORDER,
                TRACE_ORDER,
                GO_STRAIGHT_ORDER,
                RESTART_ORDER,
                EXIT_ORDER,
            ) = range(0,5)

            # capture.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH,160)
            # capture.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT,120)
            
            order_id = WAIT_ORDER
            last_frame_order_id = WAIT_ORDER
            
            while endFlag is False:
                with open("/home/user/engine","r") as f:
                    order_id = int(f.readline())
                    logging.debug(order_id)
                    if last_frame_order_id !=  order_id:
                        if last_frame_order_id == TRACE_ORDER:
                            cv2.destroyAllWindows()
                            bot.driveTest(0,0)

                        if last_frame_order_id == GO_STRAIGHT_ORDER:
                            bot.driveTest(0,0)

                        if order_id == GO_STRAIGHT_ORDER:
                            bot.driveTest(-100,0)                            

                    
                start = time.time()

                while time.time() - start < 1:
                    if order_id == TRACE_ORDER:
                        red_angle = get_red_object(capture) or 0
                        v,r = 200,-red_angle * 3.5
                        if red_angle == 0:
                            v,r = 0,0
                        bot.driveTest(v,r)
                        

                    if order_id == EXIT_ORDER:
                        endFlag = True
                        break
                    last_frame_order_id = order_id

            bot.driveTest(0,0)
            cv2.destroyAllWindows()
            logging.debug("waiting-exit")
        
    except KeyboardInterrupt:
        logging.debug('-- Ctrl-C pushed --')
        
    finally:
        capture.release()
        if windowFlag:
            cv2.destroyAllWindows()

        bot.driveTest(0,0)
        # exit()
        bot.setSafeMode()    
        bot.setForwardSpeed(0)
        bot.setTurnSpeed(0)
        bot.close()
        logging.debug("trace_red-d.py finished")
        
# def fork():
#         pid = os.fork()
#         if pid > 0:
#             sys.exit()

#         if pid == 0:
#             trace()
#             sys.exit()



if __name__ == '__main__':

    if len(sys.argv) < 3:
        print 'usage: python trace_red-d.py <port_name> start|stop windowOn'
        exit()
        
    logging.basicConfig(
        format='[%(threadName)s][%(levelname)s]%(message)s',
        levpel=logging.DEBUG
    )

    global port_name
    global startFlag
    global windowFlag

    port_name = sys.argv[1]
    startFlag = True if sys.argv[2] == "start" else None
    windowFlag = True if sys.argv[3] == "windowOn" else False
    
    if startFlag == None:
        startFlag = False if sys.argv[2] == "stop" else None

    if startFlag == None:
        print 'usage: python trace_red.py <port_name> start|stop'
        print 'order isn\'t start|stop'
        exit()

    # fork()
    bot = Robot(port_name,115200)
    
    capture = cv2.VideoCapture(0)
    if capture.isOpened() == False:
        raise("IO Error")
        sys.exit()

    # bot.setCleanMode()
    trace(bot,capture)
    
