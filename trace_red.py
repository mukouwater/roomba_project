
# coding: utf-8

import threading
import time
import datetime
import signal
import logging
from breezycreate2 import Robot

import cv2
import time
import numpy as np
from red import find_rect_of_target_color
from red import find_rect_of_blue
blue_cnt = 0

def get_red_object(capture):
    # capture = cv2.VideoCapture(1)
    angle = None
    
    if cv2.waitKey(30) < 0:
        _, frame = capture.read()
        rects = find_rect_of_target_color(frame)

        w = float(capture.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
        h = float(capture.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
    
        if len(rects) > 0:
          rect = max(rects, key=(lambda x: x[2] * x[3]))
          if rect[2] * rect[3] > 100:
              cv2.rectangle(frame, tuple(rect[0:2]), tuple(rect[0:2] + rect[2:4]), (0, 0, 255), thickness=2)
              m = rect[0:2] + rect[2:4] / 2
              cv2.rectangle(frame, tuple(m - np.array([10,10])), tuple(m + np.array([10,10])), (0, 255, 0), thickness=2)
              angle = m[0] / w - 0.5

        logging.debug(angle)
        
        global blue_cnt
        rects_blue = find_rect_of_blue(frame)
        if len(rects_blue) > 0:
          rect = max(rects_blue, key=(lambda x: x[2] * x[3]))
          if rect[2] * rect[3] > 2500:
              cv2.rectangle(frame, tuple(rect[0:2]), tuple(rect[0:2] + rect[2:4]), (255, 0, 0), thickness=2)
              blue_cnt += 1
          else:
              blue_cnt = 0
        if windowFlag:
            cv2.imshow('red', frame)
    return angle

global windowFlag 
if __name__ == '__main__':

    import sys
    if len(sys.argv) <= 2:
        print 'usage: python trace_red.py <port_name> start|stop'
        exit()
    logging.basicConfig(
        format='[%(threadName)s][%(levelname)s]%(message)s',
        level=logging.DEBUG
    )
        
    # capture = 0
    capture = cv2.VideoCapture(0)
    if capture.isOpened() == False:
        exit()
    # capture.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH,160)
    # capture.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT,120)
    port_name = sys.argv[1]

    print type(sys.argv[2].decode('cp932').encode('utf-8'))
    print type("start")
    startFlag = True if sys.argv[2] == "start" else None
    global windowFlag
    windowFlag = True if sys.argv[3] == "windowOn" else False
    if startFlag == None:
        startFlag = False if sys.argv[2] == "stop" else None

    if startFlag == None:
        print 'usage: python trace_red.py <port_name> start|stop'
        print 'order isn\'t start|stop'
        exit()

    trace_time = int(sys.argv[4])
    
    bot = Robot(port_name,115200)
    global blue_cnt
    try:
        if startFlag:
            start = time.time()
            while time.time() - start < trace_time:
                red_angle = get_red_object(capture) or 0
                v,r = 80,-red_angle * 3
                if red_angle == 0:
                    v,r = 0,0

                bot.driveTest(v,r)
                
                # if blue_cnt > 20:
                #     # bot.driveTest(0,0)
                #     # bot.setSafeMode()
                #     # time.sleep(2)
                #     # bot.setSpotMode()
                #     # time.sleep(10)

                #     bot.setSafeMode()
                #     blue_cnt = 0
                #     break

        bot.driveTest(0,0)
        
    except KeyboardInterrupt:
        logging.debug('-- Ctrl-C pushed --')
        
    finally:
        capture.release()
        if windowFlag:
            cv2.destroyAllWindows()

        bot.driveTest(0,0)
        bot.setSafeMode()    
        bot.setForwardSpeed(0)
        bot.setTurnSpeed(0)
        bot.close()

