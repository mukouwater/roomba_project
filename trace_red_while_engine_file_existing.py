# coding: utf-8

import logging
import cv2
import time
import numpy as np
import os.path
from breezycreate2 import Robot

def find_rect_of_target_color(image):
  hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV_FULL)
  h = hsv[:, :, 0]
  s = hsv[:, :, 1]
  mask = np.zeros(h.shape, dtype=np.uint8)
  mask[((h > 20) | (h < 200)) & (s > 128)] = 255
  contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
  rects = []
  for contour in contours:
    approx = cv2.convexHull(contour)
    rect = cv2.boundingRect(approx)
    rects.append(np.array(rect))
  return rects


def get_red_object(capture):
    # capture = cv2.VideoCapture(1)
    angle = None
    
    if cv2.waitKey(30) < 0:
        _, frame = capture.read()
        rects = find_rect_of_target_color(frame)
        w = float(capture.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
        h = float(capture.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
    
        if len(rects) > 0:
          rect = max(rects, key=(lambda x: x[2] * x[3]))
          cv2.rectangle(frame, tuple(rect[0:2]), tuple(rect[0:2] + rect[2:4]), (0, 0, 255), thickness=2)
          m = rect[0:2] + rect[2:4] / 2
          cv2.rectangle(frame, tuple(m - np.array([10,10])), tuple(m + np.array([10,10])), (0, 255, 0), thickness=2)
          angle = m[0] / w - 0.5

        # logging.debug(angle)
        cv2.imshow('red', frame)
    return angle
    
if __name__ == '__main__':

    import sys
    if len(sys.argv) <= 2:
        print 'usage: python trace_red.py <port_name> start|stop'
        exit()
    logging.basicConfig(
        format='[%(threadName)s][%(levelname)s]%(message)s',
        level=logging.DEBUG
    )
        
    # capture = 0
    capture = cv2.VideoCapture(0)
    if capture.isOpened() == False:
        exit()
    capture.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH,160)
    capture.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT,120)
    port_name = sys.argv[1]

    print type(sys.argv[2].decode('cp932').encode('utf-8'))
    print type("start")
    startFlag = True if sys.argv[2] == "start" else None
    if startFlag == None:
        startFlag = False if sys.argv[2] == "stop" else None

    if startFlag == None:
        print 'usage: python trace_red.py <port_name> start|stop'
        print 'order isn\'t start|stop'
        
        exit()
    # Create a Create2. This will automatically try to connect to your robot over serial
    bot = Robot(port_name,115200)

    try:
        if startFlag:
            start = time.time()
            engine_file = 'engine_key'
            while os.path.exists(engine_file):
                red_angle = get_red_object(capture) or 0
                v,r = 150,-red_angle * 3
                bot.driveTest(v,r)
                time.sleep(0.15)
                # print(v,r,red_angle)

        bot.driveTest(0,0)
        
    except KeyboardInterrupt:
        logging.debug('-- Ctrl-C pushed --')
        
    finally:
        capture.release()
        cv2.destroyAllWindows()

        bot.driveTest(0,0)
        bot.setSafeMode()    
        bot.setForwardSpeed(0)
        bot.setTurnSpeed(0)
        bot.close()

