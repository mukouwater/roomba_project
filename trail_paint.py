# -*- coding: utf-8 -*-
import sys
import Tkinter


def draw_trail(trail_log_txt):
    room_size_X = 512
    room_size_Y = 512
    
    root = Tkinter.Tk()
    root.title(u"Roomba Trail")
    root.geometry(str(room_size_X) + "x" + str(room_size_Y))


    #キャンバスエリア
    canvas = Tkinter.Canvas(root, width = room_size_X, height = room_size_Y)
    canvas.create_rectangle(0, 0, room_size_X, room_size_Y, fill = 'gray', stipple = 'gray25')

    #キャンバスバインド
    canvas.place(x=0, y=0)

    import time
    import math
    from pprint import pprint
    import os
    path = "./" + str(trail_log_txt)


    while True:
        while os.path.exists(path) is False:
            time.sleep(5)
            
        with open(trail_log_txt) as f:
            lines2 = f.readlines() 
            max_x = max_y = min_x = min_y = 0.0
            px,py,pa = 0.0,0.0,0.0
            trail_points = [[py,px]]

            for line in lines2:
                if 'connect' in line:
                    continue
                l = line.split()
                d = float(l[0])
                a = float(l[1]) / 360
                w = float(l[2])
                lb= l[3]
                rb= l[4]
                previous_x,previous_y = px,py
                pa += a
                ty = d * math.sin(pa)
                tx = d * math.cos(pa)
                py += ty
                px += tx
                trail_points.append([py,px,w,lb,rb])

                max_y = max(max_y,py)
                max_x = max(max_x,px)

                min_y = min(min_y,py)
                min_x = min(min_x,px)

            widY = max_y - min_y
            widX = max_x - min_x
            scale = min(room_size_Y / float(widY),room_size_X / float(widX))
            # print(room_size_Y,float(widY),room_size_X,float(widX),scale)
            for p in trail_points:
                p[0] -= min_y
                p[1] -= min_x
                p[0] *= scale
                p[1] *= scale

            grids = get_2darray(2,2)

            # １マスのサイズ
            grid_size_Y = int(scale * room_size_Y) / len(grids)
            grid_size_X = int(scale * room_size_X) / len(grids)


            # グリッド描写
            for i in range(room_size_Y / grid_size_Y):
                for j in range(room_size_X / grid_size_X):
                    canvas.create_rectangle(j * grid_size_X, i * grid_size_Y, (j + 1) * grid_size_X, (i + 1) * grid_size_Y , fill = 'white', stipple = 'gray25')

            # ルンバのいたマスを塗りつぶし
            for i in range(len(trail_points) - 1):
                nx,ny,px,py = trail_points[i+1][1],trail_points[i+1][0],trail_points[i][1],trail_points[i][0]
                gy,gx = int(py / grid_size_Y),int(px / grid_size_X)
                canvas.create_rectangle(gx * grid_size_X, gy * grid_size_Y, (gx + 1) * grid_size_X, (gy + 1) * grid_size_Y, fill = 'green', stipple = 'gray50')
                if trail_points[3]:
                    canvas.create_rectangle(px,py, px + 5 , py + 5, fill = 'red', stipple = 'gray50')
                if trail_points[4]:
                    canvas.create_rectangle(px,py, px - 5 , py - 5, fill = 'orange', stipple = 'gray50')

                
            # ルンバの走行跡を描写
            for i in range(len(trail_points) - 1):
                nx,ny,px,py = trail_points[i+1][1],trail_points[i+1][0],trail_points[i][1],trail_points[i][0]
                canvas.create_line(nx,ny,px,py)
            time.sleep(3)
            canvas.update()
            canvas.delete('all')

        
            
def get_2darray(Ynum,Xnum):
    grids = []
    y = Ynum
    x = Xnum
    for i in range(y):
        row = []
        for j in range(x):
            row.append([(i,j)])
        grids.append(row)

    return grids

if __name__ == "__main__":
    if len(sys.argv) > 1:
        draw_trail(sys.argv[1])
    else:
        draw_trail()

