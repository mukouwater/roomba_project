# coding: utf-8

import threading
import time
import datetime
import signal
import logging
from breezycreate2 import Robot

class PollingThread(threading.Thread):

    
    """docstring for PollingThread"""

    def __init__(self,bot):
        super(PollingThread, self).__init__()
        self.bot = bot
        self._running = True
        
    def shutdown(self):
        logging.debug('-- shutdown called --')
        self._running = False
        
    def run(self):
        try:
            bot = self.bot
            f = lambda value: -(value & 0b1000000000000000) | (value & 0b0111111111111111)
            
            d = 0
            a = 0

            start = time.time()
            # while time.time() - start < 5:
            while self._running:

                try:
                    d = f(bot.getDistance())
                    a = f(bot.getAngle())
                    w = bot.getWallSensor() 
                    rb = 1 if bot.getBumpers()[0] else 0
                    lb = 1 if bot.getBumpers()[1] else 0
                except UnicodeDecodeError:
                    continue
                    
                data = str(d) + ' ' + str(a) + ' ' + str(w)+ ' ' + str(rb) + ' ' + str(lb)
                print data
                try:
                    with open('tt.txt', 'a') as wf:
                        wf.write(data + '\n')
                except Exception:
                    logging.debug('-- write error --')
                    
                time.sleep(0.15)

            logging.debug('-- polling END --')
        except Exception as e:
            logging.debug('-- some exception happened -- :' + str(e))


def clean_and_spot(speed,wait_time):
    f = lambda value: -(value & 0b1000000000000000) | (value & 0b0111111111111111)
    # bot.setForwardSpeed(+50)
    bot.setSafeMode()
    t_time = 0.0
    start_time = time.time()

    CLEAN_MODE = 1
    SPOT_MODE = 2

    bot.setCleanMode()
    bot_mode = CLEAN_MODE

    enter_clean_time = time.time()
    exit_clean_time = -1

    enter_spot_time = -1
    exit_spot_time = -1
    
    logging.debug('ML開始: ' + str(start_time))
    while (time.time() - start_time) < wait_time:
        c_time = time.time()

        if bot_mode == CLEAN_MODE:
            vw = None
            try:
                vw = bot.getViatualWall()
            except UnicodeDecodeError:
                if vw == None:
                    vw = False

            if vw:
                print 'VW検知:'
                if c_time - exit_spot_time > wait_time / 3:
                    bot.setSafeMode()
                    bot_mode = SPOT_MODE
                    enter_spot_time = c_time
                    exit_clean_time = c_time
                    bot.setSpotMode()
                    print 'BM変更: CLEAN => SPOT'

                else:
                    print 'VW検知: 最後にスポットモードを脱してからまだ10秒以上経っていません'

        elif bot_mode == SPOT_MODE:
            if c_time - enter_spot_time > 10:
                print 'BM変更: SPOT => CLEAN'
                
                bot.setSafeMode()
                time.sleep(1)
                bot.setCleanMode()
                bot_mode = CLEAN_MODE
                enter_spot_time = c_time
                exit_spot_time = c_time

    print 'ML終了: ' + str(time.time())
            
def interpret(bot,orders):

    # Create a Create2. This will automatically try to connect to your robot over serial
    for i in range(len(orders)):
        order = orders[i]
        if order == 'straight':
            speed = int(orders[i + 1])
            interval = int(orders[i + 2])
            i += 2
            
            bot.setForwardSpeed(speed)
            time.sleep(interval)
            bot.setForwardSpeed(0)

        elif order == 'turn':
            speed = int(orders[i + 1])
            interval = int(orders[i + 2])
            i += 2
            
            bot.setTurnSpeed(speed)
            time.sleep(interval)
            bot.setTurnSpeed(0)

        elif order == 'beep':
            beep_type = int(orders[i + 1])
            i += 1
            if beep_type == 1:
                bot.setCleanMode()
                time.sleep(1.6)
            elif beep_type == 2:
                bot.setSpotMode()
                time.sleep(2.0)
                
            bot.setSafeMode()

        elif order == 'seek_dock':
            wait_time = int(orders[i + 1])
            i += 1
            bot.setSeek_DockMode()
            time.sleep(wait_time)
            
        elif order == 'spot':
            wait_time = int(orders[i + 1])
            i += 1
            bot.setSpotMode()
            time.sleep(wait_time)
            
        elif order == 'clean':
            wait_time = int(orders[i + 1])
            i += 1
            bot.setCleanMode()
            time.sleep(wait_time)
            
        elif order == 'stop':
            bot.setForwardSpeed(0)
            bot.setTurnSpeed(0)
            bot.setSafeMode()

        elif order == 'custom1':
            speed = int(orders[i + 1])
            wait_time = int(orders[i+2])
            i += 2
            
            clean_and_spot(speed,wait_time)
            
        elif order == 'custom2':
            speed = int(orders[i + 1])
            wait_time = int(orders[i+2])
            i += 2

            for i in range(3):
                bot.setForwardSpeed(speed)
                time.sleep(wait_time)
                bot.setTurnSpeed(speed)
                time.sleep(wait_time)
            bot.setSpotMode()
            time.sleep(2.0)
            bot.setSafeMode()

        time.sleep(0.1)
    bot.setSafeMode()

    
if __name__ == '__main__':

    import sys
    if len(sys.argv) <= 2:
        print 'usage: python simple_deeds.py <port_name> <order1> (<order2> ... )'
        exit()

    logging.basicConfig(
        format='[%(threadName)s][%(levelname)s]%(message)s',
        level=logging.DEBUG
    )
    port_name = sys.argv[1]
    # Create a Create2. This will automatically try to connect to your robot over serial
    bot = Robot(port_name,115200)

    try:
        arr = range(-180,180,10)
        table = []
        import math
        for i in arr:
            table.append(math.sin(float(i) / 360))
        print(table)

        i = 0

        table = [50,150,50,-50,-150,-50]
        table = [180.0, -180.0 ]
        start = time.time()
        while time.time() - start < 3:
            # bot.driveTest(-200,int(table[i] * 100))
            # v,r = +110,table[i]
            v,r = 300,table[i]
            bot.driveTest(v,r)
            print(v,r,float(v) / r)
            time.sleep(2.4)
            i += 1
            i %= len(table)

        bot.driveTest(0,0)
        raise Exception('hoge')

    
        logging.debug('-- START --')            
        PollingThread(bot).start()


        orders = sys.argv[2:]
        logging.debug(orders)
        interpret(bot,orders)
        # bot.setTurnSpeed(-30)
        # time.sleep(3)
        # bot.setForwardSpeed(-30)


        logging.debug('-- STOP --')
        
    except KeyboardInterrupt:
        logging.debug('-- Ctrl-C pushed --')
        
    finally:
        bot.driveTest(0,0)
        bot.setSafeMode()    
        bot.setForwardSpeed(0)
        bot.setTurnSpeed(0)
        bot.close()

